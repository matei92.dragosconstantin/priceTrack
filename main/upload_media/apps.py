from django.apps import AppConfig


class UploadMeidaConfig(AppConfig):
    name = 'upload_media'
