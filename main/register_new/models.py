from django.db import models


# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    salary = models.IntegerField()
    status = models.BooleanField()


class Userreg(models.Model):
    username = models.CharField(max_length=20)
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)

    class Meta:
       db_table = 'dragosfinal'
