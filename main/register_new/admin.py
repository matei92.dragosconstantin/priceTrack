from django.contrib import admin
from django.contrib.auth.models import User

from .models import Employee,Userreg
from import_export.admin import ImportExportModelAdmin

#admin.site.register(Employee)
#admin.site.register(Userreg)

@admin.register(Userreg)
class UserregAdmin(ImportExportModelAdmin):
    list_display = ('username','firstname','lastname','email','password')

@admin.register(Employee)
class EmployeeAdmin(ImportExportModelAdmin):
    list_display = ('name','address','salary','status')
