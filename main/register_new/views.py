from django.contrib.auth.models import User
from django.db import IntegrityError
from django.shortcuts import render,redirect
import csv
from django.http import HttpResponse

from .models import Userreg
from price_tracker.models import PriceTrack

from price_track_altex.models import PriceTrackAltex


def register_form(request):
    if request.method == "POST":
        username = request.POST['username']
        firstname = request.POST['first_name']
        lastname = request.POST['last_name']
        email = request.POST['email']
        password = request.POST['password']
        try:
            x = User.objects.create_user(username=username,first_name=firstname,last_name=lastname,email=email,
                                         password=password)
            x.save()
        except IntegrityError:
            print("Duplicated")
            return render(request,'register_duplicated.html')
        print("user created")
        return render(request,'register_success.html')

    else:
        return render(request,'registration.html')


def csv_database_write(request):
    # Get all data from UserDetail Databse Table
    # users = Userreg.objects.all()
    # userx = User.objects.all()
    craw = PriceTrackAltex.objects.all()
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="csv_database_write.csv"'

    writer = csv.writer(response,delimiter=',')
    writer.writerow(['name','price'])
    # writer.writerow(['username','firstname','lastname','email','password'])

    # for user in userx:
    # writer.writerow([user.username,user.email,user.first_name,user.last_name,user.is_staff])
    # return response
    for user in craw:
        writer.writerow([user.link_product,user.price_product,user.mail_address])
    return response


from django.views.generic.base import TemplateView


class CSVPageView(TemplateView):
    template_name = "csv_home.html"
