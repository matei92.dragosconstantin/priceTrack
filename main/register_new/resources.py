from import_export import resources
from .models import Userreg


class UserResource(resources.ModelResource):
    class meta:
        model = Userreg
