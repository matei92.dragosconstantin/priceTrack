from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse


def home(request):
    return render(request,'first_page_app.html',
                  {'titles': 'Django','link': 'http://127.0.0.1:8000/'})  # we can use any navigator link


#def profile(request):
 #   return render(request,'first_page.html',{'titles': 'profile page','link': 'http://127.0.0.1:8000/'})

