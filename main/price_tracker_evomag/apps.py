from django.apps import AppConfig


class PriceTrackerEvomagConfig(AppConfig):
    name = 'price_tracker_evomag'
