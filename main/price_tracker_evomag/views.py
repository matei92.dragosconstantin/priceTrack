from django.shortcuts import render

# Create your views here.
import time

from django.db import IntegrityError
from django.shortcuts import render,redirect

from . import evomag_track_prices
from price_tracker.models import PriceTrack


def evomag_track(request):
    if request.method == "POST":
        link_product = request.POST['link_evomag']
        price_product = request.POST['price_product']
        # mail_address = request.POST['mail_address']

        try:

            email_user = request.user.email

            print(email_user)
            b = PriceTrack(link_product=link_product,price_product=price_product,mail_address=email_user)
            b.save()
            while True:
                pr = evomag_track_prices.track_price_evomag(link_product,price_product,email_user)
                if pr < price_product:
                    break
                else:
                    time.sleep(10)
        except IntegrityError:
            print("Duplicated")
            return redirect('/')
    elif request.method == "GET":
        link_evomag = request.GET['link']
        print(link_evomag)
        return render(request,"only_one_button_evomag.html",{'link_evomag': link_evomag})
    return render(request,"only_one_button_evomag.html")
