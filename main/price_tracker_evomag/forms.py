from django import forms

from .models import PriceTrackEvomag


class PriceTrackEvomagForm(forms.ModelForm):
    class Meta:
        model = PriceTrackEvomag
        fields = ['link_product','price_product','mail_address']


class PriceTrackEvomagSearchForm(forms.ModelForm):
    class Meta:
        model = PriceTrackEvomag
        fields = ['link_product','price_product','mail_address']
