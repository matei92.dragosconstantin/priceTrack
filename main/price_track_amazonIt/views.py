import time

from django.db import IntegrityError
from django.shortcuts import render,redirect
from django.views.generic import TemplateView,ListView

from .models import PriceTrackAmazonIt
from . import amazon_it_track_prices


def amazon_it_track(request):
    if request.method == "POST":
        link_product = request.POST['link_product']
        price_product = request.POST['price_product']
        # mail_address = request.POST['mail_address']

        try:
            email_user = request.user.email
            b = PriceTrackAmazonIt(link_product=link_product,price_product=price_product,mail_address=email_user)
            b.save()
            while True:
                pr = amazon_it_track_prices.track_price_amazon_it(link_product,price_product,email_user)
                if pr < price_product:
                    break
                else:
                    time.sleep(10)
        except IntegrityError:
            print("error occured")

    return render(request,"one_button_amazonTrack.html")
