from django import forms

from .models import PriceTrack


class PriceTrackForm(forms.ModelForm):
    class Meta:
        model = PriceTrack
        fields = ['link_product','price_product','mail_address']


class PriceTrackSearchForm(forms.ModelForm):
    class Meta:
        model = PriceTrack
        fields = ['link_product','price_product','mail_address']
