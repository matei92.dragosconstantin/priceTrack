"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import include,url
from django.conf.urls.static import static
from django.conf import settings

# from register_new.views import export_users
# from register_new.views import csv_database_write
from register_new.views import csv_database_write

urlpatterns = [
    path('admin/',admin.site.urls),
    path('',include('start_page.urls')),
    path('register_new/',include('register_new.urls')),
    path('login/',include('login.urls')),
    path('logout/',include('logout.urls')),
    path('export/csv-database-write/',csv_database_write,name='csv_database_write'),
    path('upload_media/',include('upload_media.urls')),
    path('crawler/',include('crawler.urls')),
    # path('',include('crawler.urls')),
    path('price_tracker/',include('price_tracker.urls')),
    path('price_tracker_evomag/',include('price_tracker_evomag.urls')),
    path('price_track_amazonIt/',include('price_track_amazonIt.urls')),
    path('paypal/',include('paypal.standard.ipn.urls')),
    path('price_track_altex/',include('price_track_altex.urls')),

]

urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
