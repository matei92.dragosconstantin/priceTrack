from django.apps import AppConfig


class PriceTrackAltexConfig(AppConfig):
    name = 'price_track_altex'
