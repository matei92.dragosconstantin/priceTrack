from django import forms

from .models import PriceTrackAltex


class PriceTrackAltexForm(forms.ModelForm):
    class Meta:
        model = PriceTrackAltex
        fields = ['link_product','price_product','mail_address']


class PriceTrackAltexSearchForm(forms.ModelForm):
    class Meta:
        model = PriceTrackAltex
        fields = ['link_product','price_product','mail_address']
