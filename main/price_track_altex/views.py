import time

from django.db import IntegrityError
from django.shortcuts import render

#from ..price_tracker import altex_track_prices
from . import altex_track_prices
from .models import PriceTrackAltex
from price_tracker.models import PriceTrack


def altex_track(request):
    if request.method == "POST":
        link_product = request.POST['link_altex']
        price_product = request.POST['price_product']
        # mail_address = request.POST['mail_address']
        print('hello altex!')
        try:
            email_user = request.user.email
            b = PriceTrack(link_product=link_product,price_product=price_product,mail_address=email_user)
            b.save()
            while True:
                price_altex = altex_track_prices.track_price_altex(link_product,price_product,email_user)
                if price_altex < price_product:
                    break
                else:
                    time.sleep(10)
        except IntegrityError:
            print("error occured")
    elif request.method == "GET":
        link_altex = request.GET['link']
        print(link_altex)
        return render(request,"only_one_button_altex.html",{'link_altex': link_altex})

    return render(request,"only_one_button_altex.html")
