from django import forms

from .models import Emag,Evomag,AllSites,Altex


class EmagForm(forms.ModelForm):
    class Meta:
        model = Emag
        fields = ['link_emag','name_emag','price_emag']


class EmagSearchForm(forms.ModelForm):
    class Meta:
        model = Emag
        fields = ['link_emag','name_emag','price_emag']


class EvomagForm(forms.ModelForm):
    class Meta:
        model = Evomag
        fields = ['link_evomag','name_evomag','price_evomag']


class EvomagSearchForm(forms.ModelForm):
    class Meta:
        model = Evomag
        fields = ['link_evomag','name_evomag','price_evomag']


class AllSitesForm(forms.ModelForm):
    class Meta:
        model = AllSites
        fields = ['link','name','price']


class AllSitesSearchForm(forms.ModelForm):
    class Meta:
        model = AllSites
        fields = ['link','name','price']


class AltexForm(forms.ModelForm):
    class Meta:
        model = Altex
        fields = ['link_altex','name_altex','price_altex']


class AltexSearchForm(forms.ModelForm):
    class Meta:
        model = Altex
        fields = ['link_altex','name_altex','price_altex']
