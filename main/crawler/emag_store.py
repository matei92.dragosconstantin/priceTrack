import re
import time
from decimal import Decimal

import numpy as np
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

from .models import Emag,AllSites


def get_urls(url_prefix,url_suffix,searches):
    result = []
    for text in searches:
        result.append(f"{url_prefix}{text.lower()}{url_suffix}")
    return result


def crawl_emag(url_prefix,url_suffix,searches):
    global pret,value
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options,executable_path='D:\chromeDriver\geckodriver.exe')
    for url in get_urls(url_prefix,url_suffix,searches):
        driver.get(url)
        time.sleep(2)
        driver.find_element(By.XPATH,
         "/html/body/div[3]/div[2]/div/section[1]/div/div[3]/div[2]/div[2]/div[4]/div/div[3]/div/button").click()
        time.sleep(2)
        driver.find_element(By.XPATH,
            "/html/body/div[3]/div[2]/div/section[1]/div/div[3]/div[2]/div[2]/div[4]/div/div[3]/div/div/ul/li[3]/a").click()
        time.sleep(2)
        try:
            grila = driver.find_element_by_id("card_grid")
            lista_anunturi = grila.find_elements_by_class_name("card-section-wrapper")
            count = 0
            for anunt in lista_anunturi:
                details_link = anunt.find_element_by_class_name("product-title")
                link = details_link.get_attribute("href")

                site = driver.title
                matching_site = re.search('eMAG.ro$',site)
                result_site = matching_site.group(0)

                titlu = details_link.get_attribute("title")

                pret = anunt.find_element_by_class_name("product-new-price").text
                # tail = pret.split(" ",1)
                # substring = tail[0]
                # tail1 = re.sub(r'(\d+)',r'\1.0',substring)

                # value = float(tail1)
                print(titlu,pret)
                if Emag.objects.filter(link_emag=link,name_emag=titlu,price_emag=pret,site_emag=result_site):
                    print("Duplicates!")
                else:
                    b = Emag(link_emag=link,name_emag=titlu,price_emag=pret,site_emag=result_site)
                    b.save()
                count += 1
            print(count)

        except NoSuchElementException:
            crawl_emag(url_prefix,url_suffix,searches)
            print("no such element exception!!")
    driver.close()
    return pret


def crawl_second(url_prefix,url_suffix1,searches):
    global pret,value
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options,executable_path='D:\chromeDriver\geckodriver.exe')
    for url in get_urls(url_prefix,url_suffix1,searches):
        driver.get(url)
        time.sleep(2)
        # driver.find_element(By.XPATH,
        #  "/html/body/div[3]/div[2]/div/section[1]/div/div[3]/div[2]/div[2]/div[4]/div/div[3]/div/button").click()
        # time.sleep(2)
        # driver.find_element(By.XPATH,
        #   "/html/body/div[3]/div[2]/div/section[1]/div/div[3]/div[2]/div[2]/div[4]/div/div[3]/div/div/ul/li[3]/a").click()
        # time.sleep(2)

        try:
            grila = driver.find_element_by_id("card_grid")
            lista_anunturi = grila.find_elements_by_class_name("card-section-wrapper")
            count = 0
            for anunt in lista_anunturi:
                details_link = anunt.find_element_by_class_name("product-title")
                link = anunt.append(details_link.get_attribute("href"))
                site = driver.title
                titlu = details_link.get_attribute("title")
                pret = anunt.find_element_by_class_name("product-new-price").text
                # tail = pret.split(" ")
                # result = re.search(r'(\d+(?:\.\d+)?)',pret)
                # value = float(result)
                # x = result.group(1)
                # value = Decimal(x)

                # if Emag.objects.filter(link=link,name=titlu,price=pret,site=site):
                # print("Duplicates!")
                # else:
                # b = Emag(link=link,name=titlu,price=dec,site=result_site_evomag)
                # b.save()

                count += 1
            print(count)

        except NoSuchElementException:
            crawl_second(url_prefix,url_suffix1,searches)
            print("no such element exception!!")
    driver.close()
    return value
