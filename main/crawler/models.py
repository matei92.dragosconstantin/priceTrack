from datetime import date

from django.db import models
from django.utils import timezone
from django.utils.datetime_safe import datetime


class Emag(models.Model):
    link_emag = models.TextField()
    name_emag = models.CharField(max_length=255)
    price_emag = models.CharField(max_length=200)
    site_emag = models.TextField()
    date_emag = models.DateTimeField(default=datetime.now(),blank=True)

    def __str__(self):
        return f"{self.name_emag}"

    class Meta:
        db_table = 'emag'

class Altex(models.Model):
        link_altex = models.TextField()
        name_altex = models.CharField(max_length=255)
        price_altex = models.CharField(max_length=200)
        site_altex = models.TextField()
        date_altex = models.DateTimeField(default=datetime.now(),blank=True)

        def __str__(self):
                return f"{self.name_altex}"

        class Meta:
            db_table = 'altex'


# Abstract.user
class Evomag(models.Model):
    link_evomag = models.TextField()
    name_evomag = models.TextField()
    price_evomag = models.CharField(max_length=200)
    site_evomag = models.TextField()
    date_evomag = models.DateTimeField(default=datetime.now(),blank=True)

    def __str__(self):
        return f"{self.name_evomag}"

    class Meta:
        db_table = 'evomag'


class AllSites(models.Model):
    link = models.TextField()
    name = models.TextField()
    price = models.CharField(max_length=200)
    site = models.TextField()
    date_crawl = models.DateTimeField(default=datetime.now(),blank=True)

    def __str__(self):
        return f"{self.name} +{self.price} "

    class Meta:
        db_table = 'allsites'
