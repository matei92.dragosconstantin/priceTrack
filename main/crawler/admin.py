from django.contrib import admin

from .models import Emag,Evomag,AllSites,Altex

admin.site.register(Emag)
admin.site.register(Evomag)
admin.site.register(AllSites)
admin.site.register(Altex)
