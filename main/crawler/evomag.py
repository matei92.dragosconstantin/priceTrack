import re
import time
from decimal import Decimal
from re import sub

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from .models import Evomag,AllSites


def get_urls(url_prefix,url_suffix,searches):
    result = []
    for text in searches:
        result.append(f"{url_prefix}{text.lower()}{url_suffix}")
    return result


def crawl_evomag(url_prefix,url_suffix,searches):
    global pret
    print("in driver")
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options,executable_path='D:\chromeDriver\geckodriver.exe')
    for url in get_urls(url_prefix,url_suffix,searches):
        driver.get(url)
        try:
            grila = driver.find_element_by_class_name("product_grid")
            lista_anunturi = grila.find_elements_by_class_name("nice_product_item")

            count = 0
            for anunt in lista_anunturi:
                details_link = anunt.find_element_by_class_name("npi_name a")
                link = details_link.get_attribute("href")

                site = driver.title
                matching_site = re.search('evoMAG.ro$',site)
                result_site_evomag = matching_site.group(0)

                titlu = anunt.find_element_by_class_name("npi_name").text

                pret = anunt.find_element_by_class_name("real_price").text
                # tail = pret.split(" ",1)
                # substring = tail[0]
                # substring = substring.replace('.','')
                # dec = float(substring.replace(",","."))
                # print(link,titlu,pret)
                if Evomag.objects.filter(link_evomag=link,name_evomag=titlu,price_evomag=pret,site_evomag=result_site_evomag):
                    print("Duplicates")
                else:
                    b = Evomag(link_evomag=link,name_evomag=titlu,price_evomag=pret,site_evomag=result_site_evomag)
                    b.save()

                count += 1

            print(count)

        except NoSuchElementException:
            crawl_evomag(url_prefix,url_suffix,searches)
            print("no such element exception!!")
    driver.close()
    return pret


'''
def crawl_second(url_prefix,url_suffix,searches):
    global pret
    options = Options()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options,executable_path='D:\chromeDriver\geckodriver.exe')
    for url in get_urls(url_prefix,url_suffix,searches):
        driver.get(url)

        # driver.find_element(By.XPATH,
        #  "/html/body/div[3]/div[2]/div/section[1]/div/div[3]/div[2]/div[2]/div[4]/div/div[3]/div/button").click()
        # time.sleep(2)
        # driver.find_element(By.XPATH,
        #   "/html/body/div[3]/div[2]/div/section[1]/div/div[3]/div[2]/div[2]/div[4]/div/div[3]/div/div/ul/li[3]/a").click()
        # time.sleep(2)

        try:
            grila = driver.find_element_by_id("card_grid")
            lista_anunturi = grila.find_elements_by_class_name("card-section-wrapper")
            count = 0
            for anunt in lista_anunturi:
                details_link = anunt.find_element_by_class_name("product-title")
                # anunt.append(details_link.get_attribute("href"))
                titlu = details_link.get_attribute("title")
                pret = anunt.find_element_by_class_name("product-new-price").text
                print(titlu)
                print(pret)
                count += 1
            print(count)

        except NoSuchElementException:
            crawl_first(url_prefix,url_suffix,searches)
            print("no such element exception!!")
    driver.close()
    return pret
'''
