from django.urls import path
from . import views
from .views import SearchResultsView,HomePageView

urlpatterns = [
    path('emag/',views.emag,name='emag'),
    path('results/',SearchResultsView.as_view(),name='results'),
    path('search/',HomePageView.as_view(),name='search'),
    path('evomag_store/',views.evomag_store,name='evomag_store'),
    path('altex/',views.altex,name='altex'),
    path('all_sites/',views.all_sites,name='all_sites'),
    # path('megamain_emag/', views.megamain_emag, name='megamain_emag'),
    # path('megamain_altex/', views.megamain_altex, name='megamain_altex'),

]
