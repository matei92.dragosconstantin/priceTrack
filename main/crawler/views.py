import operator
from itertools import chain

from django.db.models import Q
from django.shortcuts import render,redirect
from django.views.generic import TemplateView,ListView

from . import emag_store,evomag,altex_store
from .models import Emag,Evomag,AllSites,Altex


def incrementNumberIn(string):
    result = ""
    numberStr = ""
    i = len(string) - 1
    while i > 0:
        c = string[i]
        if not c.isdigit():
            break
        numberStr = c + numberStr
        i -= 1
    number = int(numberStr)
    number += 1
    result += string[0:i + 1]
    result += "0" if number < 10 else ""
    result += str(number)
    return result


def emag(request):
    if request.method == "POST":
        print("post")
        searches = ["samsung"]
        url_prefix = "https://www.emag.ro/search/telefoane-mobile/"
        url_suffix = "/c?ref=autosuggest_category1"
        emag_store.crawl_emag(url_prefix,url_suffix,searches)
        # url_suffix1 = "/p01"
        # for i in range(4):
        # url_suffix1 = incrementNumberIn(url_suffix1)
        # emag_store.crawl_second(url_prefix,url_suffix1,searches)
    return render(request,'start_script_emag.html')


def evomag_store(request):
    if request.method == "POST":
        print("enters")
        searches = ["samsung"]
        url_prefix = "https://www.evomag.ro/solutii-mobile-telefoane-mobile/"
        url_suffix = ""
        print('Here')
        evomag.crawl_evomag(url_prefix,url_suffix,searches)

    return render(request,"start_script_evomag.html")


'''
        samsung = emag_store.crawl_first(url_prefix,url_suffix,searches)
        searches = ["samsung"]
        url_prefix = "https://www.emag.ro/search/telefoane-mobile/"
        url_suffix1 = "/p01"
        for i in range(2):
            url_suffix1 = incrementNumberIn(url_suffix1)
            emag_store.crawl_first(url_prefix,url_suffix1,searches)
'''


def altex(request):
    if request.method == "POST":
        print("enters")
        searched_item = input('search...:')
        searches = [searched_item]
        url_prefix = "https://altex.ro/cauta/?q="
        url_suffix = ""
        print('Here')
        altex_store.crawl_altex(url_prefix,url_suffix,searches)

    return render(request,"start_script_altex.html")


def all_sites(request):
    if request.method == "POST":
        print("enters")
        searched_item = request.POST.get('q')
        searches = [searched_item]
        url_prefix_altex = "https://altex.ro/cauta/?q="
        url_suffix_altex = ""
        url_prefix_emag = "https://www.emag.ro/search/"
        url_suffix_emag = "/c?ref=autosuggest_category1"
        print('Here')
        url_prefix_evomag = "https://www.evomag.ro/?sn.q="
        url_suffix_evomag = ""
        print('Here')
        altex_store.crawl_altex(url_prefix_altex,url_suffix_altex,searches)
        emag_store.crawl_emag(url_prefix_emag,url_suffix_emag,searches)
        evomag.crawl_evomag(url_prefix_evomag,url_suffix_evomag,searches)
        return redirect('search')
    return render(request,"search_all.html")


class HomePageView(TemplateView):
    template_name = 'search_product.html'


class SearchResultsView(ListView):
    model = Emag,Altex,Evomag
    template_name = 'result_search.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        altex_list = Altex.objects.filter(
            Q(name_altex__icontains=query) | Q(price_altex__icontains=query))
        emag_list = Emag.objects.filter(
            Q(name_emag__icontains=query) | Q(price_emag__icontains=query))
        evomag_list = Evomag.objects.filter(
            Q(name_evomag__icontains=query) | Q(price_evomag__icontains=query))

        object_list = list(chain(altex_list,emag_list,evomag_list))
        return object_list


'''
    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = AllSites.objects.filter(
            Q(name__icontains=query) | Q(price__icontains=query) | Q(site__icontains=query))

        return object_list

    '''

'''
def megamain_emag(request):
    context = {}
    if request.method == "POST":
        context['emag_categories'] = megamenu.get_emag_categories()
    return render(request,"megamenu.html",context)


def megamain_altex(request):
    context = {}
    if request.method == "POST":
        context['altex_categories'] = menu_altex.get_altex_categories()
    return render(request,"megamenu.html",context)

'''
