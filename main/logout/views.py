from django.contrib import auth
from django.shortcuts import render,redirect


# Create your views here.
def logout_form(request):
    auth.logout(request)
    return redirect('/')