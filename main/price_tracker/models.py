from django.db import models


# Create your models here.

class PriceTrack(models.Model):
    link_product = models.TextField()
    price_product = models.CharField(max_length=200)
    mail_address = models.CharField(max_length=200)

    def __str__(self):
        return self.link_product

    class Meta:
        db_table = 'price_track'
