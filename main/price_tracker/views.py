import time

from django.db import IntegrityError
from django.shortcuts import render,redirect

from . import emag_track_prices
from .models import PriceTrack


def emag_track(request):
    if request.method == "POST":
        link_product = request.POST['link_track']
        price_product = request.POST['price_product']
        # mail_address = request.POST['mail_address']

        try:

            email_user = request.user.email

            print(email_user)
            b = PriceTrack(link_product=link_product,price_product=price_product,mail_address=email_user)
            b.save()
            while True:
                pr = emag_track_prices.track_price_emag(link_product,price_product,email_user)
                if pr < price_product:
                    break
                else:
                    time.sleep(10)
        except IntegrityError:
            print("Duplicated")
            return redirect('/')
    elif request.method == "GET":
        link_track = request.GET['link']
        print(link_track)
        return render(request,"only_one_button.html",{'link_track': link_track})
    return render(request,"only_one_button.html")

