from django.http import HttpResponse
from django.shortcuts import render,redirect


# Create your views here.
def login_form(request):
    if request.method == "POST":
        username1 = request.POST['username']
        password1 = request.POST['password']
        from django.contrib import auth

        user = auth.authenticate(username=username1,password=password1)
        auth.login(request,user)
        if user is not None:
            #response = redirect('/price_tracker/altex_track/')
            #return response
            return render(request,'search_all.html')

        else:
            print("Duplicated")
            return render(request,'authentification.html')


    else:
        return render(request,'authentification.html')
